package eu.transkribus.client.oidc;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.AuthorizationUrlBuilder;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class SsoTest {
    private static final Logger logger = LoggerFactory.getLogger(SsoTest.class);
    public static void main(String... args) throws InterruptedException, IOException, ExecutionException {
//        flowWithJersey();
        flowWithScribe();
    }

//    private static void flowWithJersey() throws IOException, InterruptedException {
//        ClientIdentifier clientId = new ClientIdentifier("transkribus-swt-gui", "");
//        OAuth2CodeGrantFlow.Builder builder =
//                OAuth2ClientSupport.authorizationCodeGrantFlowBuilder(clientId,
//                        "https://account.readcoop.eu/auth/realms/readcoop/protocol/openid-connect/auth",
//                        "https://account.readcoop.eu/auth/realms/readcoop/protocol/openid-connect/token");
//
//        final String state = UUID.randomUUID().toString();
//        OAuth2CodeGrantFlow flow = builder
//                .property(OAuth2CodeGrantFlow.Phase.AUTHORIZATION, "access_type", "online")
//                .property(OAuth2CodeGrantFlow.Phase.AUTHORIZATION, "state", state)
//                .scope("openid")
//                .redirectUri("http://localhost:8080")
//                .build();
//        String authorizationUri = flow.start();
//
//        //start listener on localhost
//        CallbackListener listener = new CallbackListener("https://account.readcoop.eu/auth/realms/readcoop/protocol/openid-connect/delegated");
//        listener.start();
//
//        //open login page in browser
//        Desktop.getDesktop().browse(URI.create(authorizationUri));
//        //wait for response and stop the listener
//        listener.await();
//        listener.stop();
//
//        logger.debug("original state = {}, state from keycloak = {}", state, listener.getState());
//
//        final TokenResult result = flow.finish(listener.getCode(), listener.getState());
//        logger.debug("Access Token: {}", result.getAccessToken());
//        logger.debug("Refresh Token: {}", result.getRefreshToken());
//
//    }

    public static void flowWithScribe() throws IOException, InterruptedException {
        // Replace these with your own api key, secret, callback, base url and realm
        final String clientId = "transkribus-swt-gui"; //actually clientId
        final String callback = "http://localhost:8080";
        final String baseUrl = "https://account.readcoop.eu/auth/realms/readcoop";

        final String state = UUID.randomUUID().toString();

        AuthorizationUrlBuilder urlBuilder;
        try (OAuth20Service service = new ServiceBuilder(clientId)
                .defaultScope("openid")
                .callback(callback)
                //is there a way to embed a secret in the desktop app?
                .apiSecretIsEmptyStringUnsafe()
                .build(KeycloakApi20.instance(baseUrl))) {

            final Map<String, String> additionalParams = new HashMap<>();
            additionalParams.put("access_type", "online");

            urlBuilder = service.createAuthorizationUrlBuilder()
                    .state(state)
                    .additionalParams(additionalParams)
                    .initPKCE();
        }
        final String authorizationUrl = urlBuilder.build();
        logger.debug("Authorization URL: {}", authorizationUrl);

        //start listener on localhost
        CallbackListener listener = new CallbackListener("https://account.readcoop.eu/auth/realms/readcoop/protocol/openid-connect/delegated");
        listener.start();

        //open login page in browser
        Desktop.getDesktop().browse(URI.create(authorizationUrl));
        //wait for response and stop the listener
//        listener.await();
        listener.stop();
        Thread.sleep(60000);
//        final String code = listener.getCode();
//        logger.debug("Code = {}", code);
//
//        final OAuth2AccessToken accessToken = service.getAccessToken(AccessTokenRequestParams.create(code)
//                .pkceCodeVerifier(urlBuilder.getPkce().getCodeVerifier()));
//        logger.debug("Access token: {}", accessToken.getRawResponse());

//        // Now let's go and ask for a protected resource!
//        System.out.println("Now we're going to access a protected resource...");
//        final OAuthRequest request = new OAuthRequest(Verb.GET, protectedResourceUrl);
//        service.signRequest(accessToken, request);
//        try (Response response = service.execute(request)) {
//            System.out.println("Got it! Lets see what we found...");
//            System.out.println();
//            System.out.println(response.getCode());
//            System.out.println(response.getBody());
//        }
//        System.out.println();
//        System.out.println("Thats it man! Go and build something awesome with ScribeJava! :)");

    }

}
