package eu.transkribus.client.oidc;

import org.junit.Assert;
import org.junit.Test;

public class CallbackUtilsTest {

    @Test
    public void testFindFreePort() {
        for(int i = 0; i < 20; i++) {
            final int port = CallbackUtils.findFreePort();
            //should be in ephemeral port range
            Assert.assertTrue(port >= 1024);
            Assert.assertTrue(port <= 65535);
        }
    }
}
