package eu.transkribus.client.oidc;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CallbackListenerTest {
    private static final Logger logger = LoggerFactory.getLogger(CallbackListenerTest.class);
    /**
     * test the CallbackListener's capability to receive a code transmitted by a HTTP GET request and auto-shutdown after doing so
     */
    @Test
    public void testCallback() {
        final String code = "myCode";
        //configure without a redirect
        final String redirectUrl = null;
        final CallbackListener callback = new CallbackListener(redirectUrl);
        callback.start();
        Client client = ClientBuilder.newClient();
        Assert.assertNotNull(callback.getListenAddress());

        WebTarget target = client
                .target(callback.getListenAddress())
                .queryParam("code", code);
        Response response = target.request().get();
        Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatus());
        Assert.assertEquals(code, callback.getCode());
        callback.stop();
        Assert.assertNull(callback.getListenAddress());
        logger.debug("Listener server state = {}", callback.server.getState());
        Assert.assertTrue(callback.server.isStopped());
    }

    /**
     * Test the CallbackListener's capability to redirect incoming GET requests to another URL
     */
    @Test
    public void testRedirect() {
        final int redirectLocalhostPort = 8180;
        final String redirectedCode = "redirectedCode";
        //configure a redirect containing a parameter we can validate in another CallbackListener
        final String redirectUrl = "http://localhost:" + redirectLocalhostPort + "?code=" + redirectedCode;
        final CallbackListener callback = new CallbackListener(redirectUrl);
        callback.start();

        //Start second callback listener for validating the redirect
        final String dontRedirect = null;
        final CallbackListener redirectCallback = new CallbackListener(redirectLocalhostPort, dontRedirect);
        redirectCallback.start();

        Client client = ClientBuilder.newClient();
        WebTarget target = client
                .target(callback.getListenAddress())
                //this code may now be anything
                .queryParam("code", "anything");
        Response response = target.request().get();
        //assert that the code from the redirectUrl is received in the redirect location
        Assert.assertEquals(redirectedCode, redirectCallback.getCode());
        redirectCallback.stop();
        callback.stop();
    }
}
