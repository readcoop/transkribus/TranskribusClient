package eu.transkribus.client.oidc;

import eu.transkribus.client.util.SessionExpiredException;

import java.io.IOException;

public class TrpSsoHandlerTest {

    public static void main(String[] args) throws IOException, InterruptedException, SessionExpiredException {
        final String clientId = "transkribus-swt-gui";
        final String baseUrlWithRealm = "https://account.readcoop.eu/authTesting/realms/readcoop";
        ITokenStorage tokenStorage = new FileTokenStorage(clientId);
        TrpSsoHandler handler = new TrpSsoHandler(clientId, baseUrlWithRealm);
        handler.login();
        for(int i = 0; i < 1; i++) {
            handler.getAccessTokenString();
            Thread.sleep(2000);
        }
        handler.revokeRefreshToken(handler.getRefreshTokenString());
    }

}
