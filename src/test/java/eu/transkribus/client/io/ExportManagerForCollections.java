package eu.transkribus.client.io;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.login.LoginException;
import javax.ws.rs.ClientErrorException;
import javax.xml.bind.JAXBException;

import org.slf4j.LoggerFactory;

import com.itextpdf.text.DocumentException;

import eu.transkribus.client.connection.TrpServerConn;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.TrpDocMetadata;
import eu.transkribus.core.model.beans.TrpPage;
import eu.transkribus.core.model.builder.ExportCache;
import eu.transkribus.core.model.builder.pdf.PdfExporter;
import eu.transkribus.core.util.CoreUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.dea.fimgstoreclient.IFimgStoreGetClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExportManagerForCollections {
	
	private static final Logger logger = LoggerFactory.getLogger(ExportManagerForCollections.class);
	static TrpServerConn conn = null;
	protected static IFimgStoreGetClient getter = null;
	
	static int totalExported = 0;
	
	public static void main(String[] args) throws LoginException, MalformedURLException, DocumentException, IOException, JAXBException, URISyntaxException, InterruptedException{
		
	
		conn = new TrpServerConn(TrpServerConn.SERVER_URIS[0], "guenter.hackl@transkribus.eu", "1q...");
		
		getter = conn.newFImagestoreGetClient();
		
		List<TrpDocMetadata> allDocs = conn.getAllDocs(70329);
		
		exportPageXMLs(allDocs);
		
		//exportPDFs(allDocs);
		

		
			

		
		//TrpRtfBuilder.writeRtfForDoc(doc4, false, new File("C:/Users/Administrator/KonzilsProtokolle_test2.rtf"), idxs2, null);
		//(new PdfExporter()).export(doc2, "C:/Users/Administrator/Reichsgericht_test.pdf", idxs2);

		//(new PdfExporter()).export(doc, "/tmp/doc107.pdf", null);

	}

	private static void exportPDFs(List<TrpDocMetadata> allDocs) {
		try {
			for (TrpDocMetadata doc : allDocs) {
				String pageString = "1-"+doc.getNrOfPages();
				logger.debug("page string is " + pageString);
				Set<Integer> pageIndices = CoreUtils.parseRangeListStr(pageString, Integer.MAX_VALUE);
				
				boolean wordbased = false;
				
				ExportCache cache = new ExportCache();
				
				TrpDoc trpDoc = conn.getTrpDoc(70329, doc.getDocId(), -1);
				
				//cache.storeCustomTagMapForDoc(trpDoc, wordbased, pageIndices, null, false);
				try {
					cache.storePageTranscripts4Export(trpDoc, pageIndices, null, "Latest", -1, null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				(new PdfExporter()).export(trpDoc, "D:\\01_Projekte\\Kataster\\PDF_Export\\"+doc.getTitle()+".pdf" , pageIndices, cache, false);
				System.in.read();
				
			}
		
		} catch (SessionExpiredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	private static void exportPageXMLs(List<TrpDocMetadata> allDocs) {
		try {
		
			for (TrpDocMetadata doc : allDocs) {
				
				String pageString = "1-"+doc.getNrOfPages();
				logger.debug("page string is " + pageString);
				Set<Integer> pageIndices = CoreUtils.parseRangeListStr(pageString, Integer.MAX_VALUE);
							
				TrpDoc trpDoc;
				
				trpDoc = conn.getTrpDoc(70329, doc.getDocId(), -1);
				
				String path = "D:\\01_Projekte\\Kataster\\Export\\"+doc.getTitle()+"\\page\\";
				File currFolder = new File(path);
				if (currFolder.exists()) {
					logger.debug("this folder already exists");
					continue;
				}

				for (TrpPage page : trpDoc.getPages()) {
					String baseName = FilenameUtils.getBaseName(page.getImgFileName());

					FileUtils.forceMkdir(currFolder);
					
					//previously getPath was toURI -> maybe this does not work - not tested!!!!!!!!
					getter.saveFile(page.getCurrentTranscript().getUrl().getPath(), path, baseName + ".xml");

				}
				
				totalExported += doc.getNrOfPages();
				
				logger.debug("doc " + doc.getTitle() + " exported. Total files: " + totalExported);
//				if (totalExported % 1000 == 0) {
//					logger.debug("");
//				}
				
				
				//System.in.read();

			}
		} catch (SessionExpiredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
}
