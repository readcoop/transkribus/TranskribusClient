package eu.transkribus.client.io;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import javax.security.auth.login.LoginException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import eu.transkribus.client.connection.TrpServerConn;
import eu.transkribus.core.io.GoobiMetsImporter;
import eu.transkribus.core.io.UnsupportedFormatException;
import eu.transkribus.core.model.beans.TrpDoc;
import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.core.model.beans.job.enums.JobType;
import eu.transkribus.core.model.builder.pdf.PdfExporter;
import eu.transkribus.core.util.XmlUtils;

/**
 * Test for the Goobi import
 * read Goobi Mets -> fetch all files into local folder -> build TrpDocument -> ingest the document
 * TODO: collection ID should be a delivered parameter
 * @author Administrator
 *
 */
public class GoobiMetsImporterTest {
	private static final Logger logger = LoggerFactory.getLogger(GoobiMetsImporterTest.class);
	
	public static void main(String[] args) {
		
			try {
				System.out.println("start_this");
				TrpServerConn conn = new TrpServerConn(TrpServerConn.SERVER_URIS[1], "user", "pwd");
				
				//String metsLink = "file:///C://01_Projekte/READ/Programmierung/Goobi_Interface/narc_mets.xml";
				//String metsLink = "file:///C:/01_Projekte/READ/Programmierung/Goobi_Interface/AC10109954.xml";
				//String metsLink = "https://visuallibrary.net/download/format/mets/356384";
				//String metsLink = "http://rosdok.uni-rostock.de/file/rosdok_document_0000007322/rosdok_derivate_0000026952/ppn778418405.dv.mets.xml";
				String metsLink = "http://dl.ub.uni-freiburg.de/diglitData2/mets/servasanctus1484.xml";
				
				//download Mets
				logger.debug("mets link " +  metsLink);
				String docPath = System.getProperty("user.home") + File.separator + "test";
				logger.debug("docPath " +  docPath);
				String metsPath = docPath + File.separator + "mets.xml";
				logger.debug("metsPath " +  metsPath);
				
				
				File metsFile = new File(metsPath);
				FileUtils.copyURLToFile(new URL(metsLink), metsFile);
				
				/*
				 * instead of searching for ("xmlns:dv").equals("http://dfg-viewer.de/") in METS we allow all valid METS files
				 * BUT they must be created with respect to the dfg-viewer standard
				 */
				URL schemaFile = new URL("https://www.loc.gov/standards/mets/version112/mets.xsd");
				if (XmlUtils.isValid(metsFile, schemaFile)){
					logger.debug("valid Mets delivered....go on");
				}
				else{
					logger.error("Not a valid METS file with respect to http://www.loc.gov/standards/mets/version112/mets.xsd!");
					return;
				}

				int collectionID = 211;

//
				//TrpDoc doc = GoobiMetsImporter.loadDocFromGoobiMets("C:/01_Projekte/READ/Programmierung/Goobi_Interface/279.xml");
//				
				GoobiMetsImporter importer = new GoobiMetsImporter();
				TrpDoc doc = importer.unmarshalMetsAndLoadDocFromGoobiMets(metsFile);
				conn.uploadTrpDoc(collectionID, doc, null);
				conn.close();
						
			} catch (LoginException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServerErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//			} catch (MalformedURLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (JAXBException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SAXException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
}
