package eu.transkribus.client.connection;
import java.util.List;

import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.rest.TrpModelMetadataList;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.client.ATrpClientTest;
import eu.transkribus.core.model.beans.ReleaseLevel;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpGroundTruthPage;
import eu.transkribus.core.util.ModelUtil;

public class HtrModelTest extends ATrpClientTest {
	private static final Logger logger = LoggerFactory.getLogger(HtrModelTest.class);
		
	@Test
	public void testGetModel() throws Exception {
		try {
			List<TrpCollection> colList = client.getCollections(null,0, -1, null, null).getList();
			Assume.assumeFalse("No collections could be retrieved!", CollectionUtils.isEmpty(colList));
			
			TrpCollection col = colList.get(0);
			final int colId = col.getColId();
			logger.info("Trying to retrieve models in collection {}", colId);
			
			TrpModelMetadataList htrList = client.getModelCalls().getModels(ModelUtil.TYPE_TEXT, colId, null, null, null, ModelUtil.PROVIDER_CITLAB_PLUS,
					null, null, null, null, null, null,0, -1, null, null);
			logger.info("Retrieved {} models from server", htrList.getList().size());
			for(TrpModelMetadata htr : htrList.getList()) {
				
				
				if(ReleaseLevel.fromString(htr.getReleaseLevel()).getValue() > 0) {
					logger.info("Found public model with ReleaseLevel = {}", htr.getReleaseLevel());
					if(ReleaseLevel.isPrivateDataSet(ReleaseLevel.fromString(htr.getReleaseLevel()))) {
						//we should not be able to retrieve the datasets for this model!
						try {
							List<TrpGroundTruthPage> gtPages = client.getModelCalls().getHtrTrainData(htr.getModelId());
							Assert.fail("Could retrieve {} gtPages for public model with private datasets!");
						} catch (Exception e) {
							//this is fine
							logger.debug("Retrieving datasets for public model ID = {} with private datasets failed as expected.", htr.getModelId(), e.getMessage());
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
}
