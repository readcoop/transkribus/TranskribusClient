package eu.transkribus.client.connection;

import eu.transkribus.client.ATrpClientTest;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.util.AuthUtils;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class PdfUploadTest extends ATrpClientTest {
	private static final Logger logger = LoggerFactory.getLogger(PdfUploadTest.class);

	@Ignore("Ignored as there is no test resource included")
	@Test
	public void testUploadPdf() throws Exception {
		try {
			List<TrpCollection> colList = client.getCollections(null,0, -1, null, null).getList();

			Optional<TrpCollection> colOpt = colList.stream()
					.filter(c -> AuthUtils.canManage(c.getRole()))
					.findFirst();
			Assume.assumeTrue("There is no collection where the upload is allowed for this user!", colOpt.isPresent());

			File pdfFile = new File("/path/to/my.pdf");

			TrpCollection c = colOpt.get();
			logger.info("Trying to upload PDF '{}' to collection: {}", pdfFile.getName(), c);

			String jobIdStr = client.uploadPdf(c.getColId(), String.format("My custom title for PDF '%s'", pdfFile.getName()), pdfFile);
			logger.info("Started job: {}", jobIdStr);
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
}
