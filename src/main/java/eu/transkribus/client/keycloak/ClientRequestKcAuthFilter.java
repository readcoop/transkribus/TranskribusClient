//package eu.transkribus.client.keycloak;
//
//import org.keycloak.adapters.ServerRequest.HttpFailure;
//import org.keycloak.adapters.installed.KeycloakInstalled;
//import org.keycloak.common.VerificationException;
//import org.keycloak.representations.AccessToken;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.ws.rs.client.ClientRequestContext;
//import javax.ws.rs.client.ClientRequestFilter;
//import javax.ws.rs.core.HttpHeaders;
//import java.io.IOException;
//import java.util.concurrent.TimeUnit;
//
//@Deprecated
//public class ClientRequestKcAuthFilter implements ClientRequestFilter {
//	private final static Logger logger = LoggerFactory.getLogger(ClientRequestKcAuthFilter.class);
//
//	private final static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
//
//	private final KeycloakInstalled keycloak;
//	// ensure token used is valid for at least 5 seconds
//	private final long minValidity = 5L;
//
//	public ClientRequestKcAuthFilter(KeycloakInstalled keycloak) {
//		this.keycloak = keycloak;
//	}
//
//	public void filter(ClientRequestContext requestContext) throws IOException {
//		String value;
//		try {
//			if(logger.isDebugEnabled()) {
//				AccessToken token = keycloak.getToken();
//				long expires = ((long) token.getExpiration()) * 1000 - TIME_UNIT.toMillis(minValidity);
//				if(expires < System.currentTimeMillis()) {
//					logger.debug("Access token is expired: {}", keycloak.getTokenString());
//					logger.debug("Retrieving new one with refreshToken: {}", keycloak.getRefreshToken());
//				}
//			}
//			value = "Bearer " + keycloak.getTokenString(minValidity, TIME_UNIT);
//		} catch (VerificationException | HttpFailure e) {
//			logger.error("Unable to retrieve access token!", e);
//			throw new IOException("Could not retrieve access token.", e);
//		}
//		requestContext.getHeaders().putSingle(HttpHeaders.AUTHORIZATION, value);
//	}
//}
