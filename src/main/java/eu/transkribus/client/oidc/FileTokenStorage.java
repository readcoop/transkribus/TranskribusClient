package eu.transkribus.client.oidc;

import eu.transkribus.core.model.beans.auth.TrpUser;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * ITokenStorage that stores refresh tokens to files in the user home. Ignores the authServerUrl identifier and will only store a single token for a user.
 * Just here for testing.
 */
public class FileTokenStorage implements ITokenStorage {
    private static final Logger logger = LoggerFactory.getLogger(FileTokenStorage.class);
    private final File preferencesDir;
    private static final String TOKEN_FILENAME = "token.properties";

    private final String clientName;

    public FileTokenStorage(String clientName) {
        if(clientName == null) {
            throw new IllegalArgumentException("clientName must not be null.");
        }
        if(!clientName.startsWith(".")) {
            clientName = "." + clientName;
        }
        this.clientName = clientName;
        this.preferencesDir = initPreferencesDir();
    }

    private File initPreferencesDir() {
        File preferencesDir = new File(System.getProperty("user.home"), clientName);
        if (preferencesDir.isDirectory() && preferencesDir.canWrite()) {
            return preferencesDir;
        }
        if (preferencesDir.mkdirs()) {
            return preferencesDir;
        }
        preferencesDir = new File("");
        logger.warn("User home is not writeable! Storing token.properties at: {}", preferencesDir.getAbsolutePath());
        return preferencesDir;
    }

    public List<String> getUsernames(final String authServerUrl) {
        Properties creds = loadProperties();
        if(creds == null) {
            return Collections.EMPTY_LIST;
        }
        List<String> usernames = creds.keySet().stream()
                .map(Object::toString)
                .sorted()
                .collect(Collectors.toList());
        return usernames;
    }

    public String getStoredRefreshToken(final String authServerUrl, final String username) {
        Properties creds = loadProperties();
        if(creds == null) {
            //token storage file does not exist
            return null;
        }
        if(username == null) {
            return null;
        }
        return creds.getProperty(username);
    }

    private Properties loadProperties() {
        Properties creds = new Properties();
        try (InputStream is = Files.newInputStream(getPropsFile().toPath())) {
            creds.load(is);
        } catch (IOException e) {
            logger.error("Could not load refresh token: {}", e.getMessage());
            return null;
        }
        return creds;
    }

    private void storeProperties(Properties creds) {
        File propsFile = getPropsFile();
        if (propsFile.isFile()) {
            FileUtils.deleteQuietly(propsFile);
        }
        try (OutputStream os = Files.newOutputStream(propsFile.toPath())) {
            creds.store(os, null);
            logger.info("Successfully stored new refresh token.");
        } catch (IOException e) {
            logger.error("Could not save refresh token", e);
        }
    }

    private File getPropsFile() {
        return new File(preferencesDir, TOKEN_FILENAME);
    }

    public void updateStoredRefreshToken(final String authServerUrl, final TrpUser user, String refreshToken) {
        Objects.requireNonNull(user);
        final String username = user.getUserName();
        Properties creds = loadProperties();
        if(creds == null) {
            creds = new Properties();
        }
        if(refreshToken == null) {
            creds.remove(username);
        } else {
            creds.setProperty(username, refreshToken);
        }
        storeProperties(creds);
    }

    @Override
    public void removeStoredRefreshToken(String authServerUrl, String username) {
        Properties creds = loadProperties();
        if(creds == null) {
            return;
        }
        creds.remove(username);
        storeProperties(creds);
    }
}
