package eu.transkribus.client.oidc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ScopeBuilder;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuth2AccessTokenErrorResponse;
import com.github.scribejava.core.oauth.AccessTokenRequestParams;
import com.github.scribejava.core.oauth.AuthorizationUrlBuilder;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.revoke.TokenTypeHint;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.model.beans.auth.TrpUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class TrpSsoHandler {
    private static final Logger logger = LoggerFactory.getLogger(TrpSsoHandler.class);

    private static final String SCOPE_DEFAULT = "openid";
    private static final String SCOPE_OFFLINE_ACCESS = "offline_access";
    private IDesktopProvider desktopProvider;
    private final int callbackPort;
    private final String keycloakBaseUrlWithRealm;
    private final DefaultApi20 authApi;
    private final OAuth20Service oauthService;
    private final JwtClaimExtractor jwtReader;
    private OAuth2AccessToken token;
    private long lastRefreshMillis = 0;
    private TrpUser currentUser;

    public TrpSsoHandler(final String clientId, final String keycloakBaseUrlWithRealm) {
        this.keycloakBaseUrlWithRealm = keycloakBaseUrlWithRealm;
        this.authApi = KeycloakApi20.instance(keycloakBaseUrlWithRealm);
        this.callbackPort = CallbackUtils.findFreePort();
        //offline_access scope => refresh tokens will be valid for 30 days when not used
        final ScopeBuilder scopeBuilder = new ScopeBuilder(SCOPE_DEFAULT, SCOPE_OFFLINE_ACCESS);
        this.oauthService = new ServiceBuilder(clientId)
                .defaultScope(scopeBuilder)
                .callback("http://localhost:" + this.callbackPort)
                //is there a way to embed a secret in the desktop app?
                .apiSecretIsEmptyStringUnsafe()
                .build(authApi);

        this.desktopProvider = new IDesktopProvider() {};
        this.jwtReader = new JwtClaimExtractor();
    }

    public void login() throws IOException, InterruptedException {
        this.login(null);
    }

    /**
     * Start authorization code flow via system browser
     * @param usernameHint if not null then this value is passed as prefill value to the username field of the login form
     */
    public void login(final String usernameHint) throws IOException, InterruptedException {
        final Map<String, String> additionalParams = new HashMap<>();
        if(usernameHint != null) {
            additionalParams.put("login_hint", usernameHint);
        }
        final String state = UUID.randomUUID().toString();

        AuthorizationUrlBuilder urlBuilder = this.oauthService.createAuthorizationUrlBuilder()
                .state(state)
                .additionalParams(additionalParams)
                .initPKCE();
        final String authorizationUrl = urlBuilder.build();
        logger.debug("Authorization URL: {}", authorizationUrl);

        final String code = retrieveAuthorizationCodeByBrowser(state, authorizationUrl);

        try {
            this.token = this.oauthService.getAccessToken(AccessTokenRequestParams.create(code)
                    .pkceCodeVerifier(urlBuilder.getPkce().getCodeVerifier()));
        } catch (InterruptedException | ExecutionException e) {
            throw new IOException("Could not retrieve access token with authorization code.", e);
        }
        logger.debug("Access token: {}", this.token.getRawResponse());
        this.currentUser = mapToUser(this.token);
        lastRefreshMillis = System.currentTimeMillis();
    }

    private TrpUser mapToUser(OAuth2AccessToken token) {
        TrpUser user = new TrpUser();
        user.setUserName(jwtReader.readClaim(token.getAccessToken(), "preferred_username"));
        user.setEmail(jwtReader.readClaim(token.getAccessToken(), "email"));
        final Integer userId = jwtReader.readIntClaim(token.getAccessToken(), "trp_user_id");
        if(userId != null) {
            user.setUserId(userId);
        }
        return user;
    }

    private String retrieveAuthorizationCodeByBrowser(String state, String authorizationUrl) throws IOException, InterruptedException {
        final String redirectUrlStr = authApi.getAccessTokenEndpoint().replace("/protocol/openid-connect/token", "/desktop-login-info");
        logger.debug("Redirect after login: {}", redirectUrlStr);

        //start listener on localhost
        CallbackListener listener = new CallbackListener(callbackPort, redirectUrlStr);
        listener.start();

        //open login page in browser
        desktopProvider.browse(URI.create(authorizationUrl));
        //wait for response and stop the listener
        try {
            listener.await();
        } catch (InterruptedException e) {
            logger.info("Waiting on callback was interrupted.");
            throw e;
        } finally {
            logger.debug("Stopping callback listener.");
            listener.stop();
        }
        final String code = listener.getCode();
        logger.debug("Code = {}", code);
        if(!state.equals(listener.getState())) {
            throw new IOException("State from authorization service does not match!");
        }
        return code;
    }

    public String getAccessTokenString() {
        return this.token.getAccessToken();
    }

    public String getRefreshTokenString() {
        return this.token.getRefreshToken();
    }

    public boolean isAccessTokenValid(long minValidity, TimeUnit unit) {
        if(this.token == null) {
            return false;
        }
        final Integer tokenLifespanSeconds = this.token.getExpiresIn();
        if(tokenLifespanSeconds == null) {
            return true;
        }
        final long expiresAtMillis = this.lastRefreshMillis + tokenLifespanSeconds * 1000;
        logger.debug("Access token expires in {} seconds", (expiresAtMillis - System.currentTimeMillis()) / 1000L);
        long refreshAtMillis = expiresAtMillis - unit.toMillis(minValidity);
        return refreshAtMillis > System.currentTimeMillis();
    }

    public String refreshToken(String refreshToken) throws IOException, SessionExpiredException {
        try {
            this.token = this.oauthService.refreshAccessToken(refreshToken);
            this.lastRefreshMillis = System.currentTimeMillis();
            final TrpUser recentUser = this.currentUser;
            this.currentUser = mapToUser(token);
            if(recentUser != null //recentUsername is null on first refresh with existing token
                    && currentUser != null
                    && currentUser.getUserId() == recentUser.getUserId()
                    && !currentUser.getUserName().equals(recentUser.getUserName())) {
                logger.info("Username has changed: {} -> {}", recentUser.getUserName(), this.currentUser.getUserName());
            }
            return this.token.getRefreshToken();
        } catch (OAuth2AccessTokenErrorResponse e) {
            //refresh token is expired or SSO session was logged out.
            logger.error("Could not refresh access token: {} - {}", e.getError(), e.getErrorDescription());
            //calling this::login here brings up a browser window and leaves swt gui in a blocked state. Handle this elsewhere.
            throw new SessionExpiredException("SSO session has expired or user logged out.");
        } catch (InterruptedException | ExecutionException e) {
            throw new IOException("Could not retrieve access token with authorization code.", e);
        }
    }

    public void revokeRefreshToken(String refreshToken) throws IOException {
        try {
            this.oauthService.revokeToken(refreshToken, TokenTypeHint.REFRESH_TOKEN);
        } catch (InterruptedException | ExecutionException e) {
            throw new IOException("Could not revoke refresh token with authorization code.", e);
        }
    }

    public TrpUser getCurrentUser() {
        return currentUser;
    }

    public String getKeycloakBaseUrlWithRealm() {
        return keycloakBaseUrlWithRealm;
    }

    public void setDesktopProvider(IDesktopProvider desktopProvider) {
        this.desktopProvider = desktopProvider;
    }

    public interface IDesktopProvider {
        default void browse(URI uri) throws IOException {
            Desktop.getDesktop().browse(uri);
        }
    }

    /**
     * Utility for extracting claims as String from a JWT access token
     */
    private static class JwtClaimExtractor {
        private final Base64.Decoder decoder;
        private final ObjectMapper mapper;
        JwtClaimExtractor() {
            decoder = Base64.getUrlDecoder();
            mapper = new ObjectMapper();
        }

        Object readClaimObject(final String jwt, final String claim) {
            final String[] chunks = jwt.split("\\.");
            final String payload = new String(decoder.decode(chunks[1]));
            try {
                Map<String, Object> map = mapper.readValue(payload, Map.class);
                return map.get(claim);
            } catch (JsonProcessingException e) {
                logger.warn("Could not extract username from access token!");
                return null;
            }
        }

        public String readClaim(final String jwt, final String claim) {
            Object v = readClaimObject(jwt, claim);
            if(v instanceof String) {
                return (String) v;
            }
            logger.debug("'{}' seems to be not a JWT String claim. Value = {}", claim, v);
            return null;
        }

        public Integer readIntClaim(final String jwt, final String claim) {
            Object v = readClaimObject(jwt, claim);
            if(v instanceof Integer) {
                return (Integer) v;
            }
            logger.debug("'{}' seems to be not a JWT int claim. Value = {}", claim, v);
            return null;
        }
    }
}