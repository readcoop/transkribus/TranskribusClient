package eu.transkribus.client.oidc;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;

public class CallbackListener {
    private static final Logger logger = LoggerFactory.getLogger(CallbackListener.class);

    /**
     * FIXME: this should rather use the loopback IP address
     * https://datatracker.ietf.org/doc/html/draft-ietf-oauth-v2-1-01#name-loopback-redirect-considera
     */
    private final static String DEFAULT_HOST = "http://localhost";
    private final static int DEFAULT_PORT = 8080;

    private final CountDownLatch shutdownSignal = new CountDownLatch(1);
    private final CallbackHandler callbackHandler;
    Server server;
    private final Integer port;
    private URI listenAddress;

    CallbackListener(String redirectOnCallback) {
        this(DEFAULT_PORT, redirectOnCallback);
    }

    CallbackListener(int port, String redirectOnCallback) {
        if(port < 1024 || port > 65535) {
            logger.warn("Port argument is outside ephemeral port range ({}). Using DEFAULT_PORT = {}", port, DEFAULT_PORT);
            port = DEFAULT_PORT;
        }
        this.port = port;
        this.callbackHandler = new CallbackHandler(redirectOnCallback, shutdownSignal);
    }

    public void start() {
        String uriStr = DEFAULT_HOST + ":" + port;
        this.listenAddress = URI.create(uriStr);
        this.server =
                JettyHttpContainerFactory.createServer(listenAddress, false);
        ContextHandler handler = new ContextHandler();
        handler.setContextPath("/");
        handler.setHandler(callbackHandler);
        this.server.setHandler(handler);
        try {
            this.server.start();
            logger.info("Started callback listener at {}", getListenAddress());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return the listen address of the callback or null if the listener is not started or stopped already.
     */
    public URI getListenAddress() {
        return listenAddress;
    }

    public void stop() {
        try {
            logger.debug("Stopping server");
            server.stop();
        } catch (Exception e) {
            logger.error("Shutdown failed.", e);
        }
        server.destroy();
        listenAddress = null;
        shutdownSignal.countDown();
    }

    public void await() throws InterruptedException {
        logger.debug("Wait on shutdown signal");
        this.shutdownSignal.await();
        logger.debug("Wait finished");
    }

    public String getCode() {
        return callbackHandler.getCode();
    }

    public String getError() {
        return callbackHandler.getError();
    }

    public String getErrorDescription() {
        return callbackHandler.getErrorDescription();
    }

    public String getState() {
        return callbackHandler.getState();
    }

    public static class CallbackHandler extends AbstractHandler {
        private final String redirectOnCallback;
        private final CountDownLatch shutdownSignal;
        private String code;
        private String error;
        private String errorDescription;
        private String state;

        public CallbackHandler(String redirectOnCallback, CountDownLatch shutdownSignal) {
            this.redirectOnCallback = redirectOnCallback;
            this.shutdownSignal = shutdownSignal;
        }

        @Override
        public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            //read data from request
            this.code = request.getParameter("code");
            this.state = request.getParameter("state");
            this.error = request.getParameter("error");
            this.errorDescription = request.getParameter("error_description");
            logger.debug("CallbackHandler read request:");
            logger.debug("Method = {}", request.getMethod());
            logger.debug("Code = {}", code);
            logger.debug("State = {}", state);
            logger.debug("Error = {}", error);
            logger.debug("Error Description = {}", errorDescription);
            response = buildResponse(request, response);
            baseRequest.setHandled(true);
            response.flushBuffer();
            this.shutdownSignal.countDown();
        }

        private HttpServletResponse buildResponse(HttpServletRequest request, HttpServletResponse response) {
            if(StringUtils.isBlank(redirectOnCallback)) {
                if(error != null) {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    response.setStatus(HttpServletResponse.SC_OK);
                }
            } else {
                if (error != null || !HttpMethod.GET.name().equals(request.getMethod())) {
                    String errorRedirect = redirectOnCallback + "?error=true";
                    response.setStatus(HttpServletResponse.SC_FOUND);
                    response.setHeader(HttpHeader.LOCATION.name(), errorRedirect);
                } else {
                    response.setStatus(HttpServletResponse.SC_FOUND);
                    response.setHeader(HttpHeader.LOCATION.name(), redirectOnCallback);
                }
            }
            return response;
        }

        public String getCode() {
            return code;
        }

        public String getError() {
            return error;
        }

        public String getErrorDescription() {
            return errorDescription;
        }

        public String getState() {
            return state;
        }
    }
}