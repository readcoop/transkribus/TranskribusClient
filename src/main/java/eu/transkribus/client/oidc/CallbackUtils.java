package eu.transkribus.client.oidc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;

public class CallbackUtils {
    private static final Logger logger = LoggerFactory.getLogger(CallbackUtils.class);

    static int findFreePort() {
        Integer port = null;
        int nrOfTries = 1;
        while(port == null) {
            try (ServerSocket serverSocket = new ServerSocket(0)) {
                port = serverSocket.getLocalPort();
            } catch (IOException e) {
                nrOfTries++;
            }
        }
        logger.debug("Determined free port after {} tries: {}", nrOfTries, port);
        return port;
    }
}
