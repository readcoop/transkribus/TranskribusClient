package eu.transkribus.client.oidc;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.github.scribejava.apis.openid.OpenIdJsonTokenExtractor;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.OAuth2AccessTokenJsonExtractor;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.httpclient.HttpClient;
import com.github.scribejava.core.httpclient.HttpClientConfig;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthAsyncRequestCallback;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.revoke.TokenTypeHint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adapted Keycloak API with configurable context path w.r.t. Keycloak versions > 19 where the default context path has changed.<br/>
 * Based on PR: <a href="https://github.com/scribejava/scribejava/pull/1044">https://github.com/scribejava/scribejava/pull/1044</a>
 * Remove this class and replace with KeycloakApi once the change is merged.
 * <br/>
 * Additional change: added revokeToken implementation using logout endpoint to emulate what KeycloakInstalled does
 */
public class KeycloakApi20 extends DefaultApi20 {
    private static final Logger logger = LoggerFactory.getLogger(KeycloakApi20.class);

    private static final ConcurrentMap<String, KeycloakApi20> INSTANCES = new ConcurrentHashMap<>();

    private final String baseUrlWithRealm;

    protected KeycloakApi20(String baseUrlWithRealm) {
        this.baseUrlWithRealm = baseUrlWithRealm;
    }

    public static KeycloakApi20 instance(final String defaultBaseUrlWithRealm) {
        //java8: switch to ConcurrentMap::computeIfAbsent
        KeycloakApi20 api = INSTANCES.get(defaultBaseUrlWithRealm);
        if (api == null) {
            api = new KeycloakApi20(defaultBaseUrlWithRealm);
            final KeycloakApi20 alreadyCreatedApi = INSTANCES.putIfAbsent(defaultBaseUrlWithRealm, api);
            if (alreadyCreatedApi != null) {
                return alreadyCreatedApi;
            }
        }
        return api;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return baseUrlWithRealm + "/protocol/openid-connect/token";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return baseUrlWithRealm + "/protocol/openid-connect/auth";
    }

    @Override
    public TokenExtractor<OAuth2AccessToken> getAccessTokenExtractor() {
        return OpenIdJsonTokenExtractor.instance();
    }

    /**
     * Keycloak < version ~17 does not include the revoke endpoint. Override to return the logout endpoint and override revoke method in service below.
     */
    @Override
    public String getRevokeTokenEndpoint() {
        //this is /revoke for newer Keycloak versions
//        return baseUrlWithRealm + "/protocol/openid-connect/revoke";
        return baseUrlWithRealm + "/protocol/openid-connect/logout";
    }

    @Override
    public OAuth20Service createService(String apiKey, String apiSecret, String callback, String defaultScope, String responseType, OutputStream debugStream, String userAgent, HttpClientConfig httpClientConfig, HttpClient httpClient) {
        return new OAuth20Service(this, apiKey, apiSecret, callback, defaultScope, responseType, debugStream, userAgent, httpClientConfig, httpClient) {
            protected OAuthRequest createRevokeTokenRequest(String tokenToRevoke, TokenTypeHint tokenTypeHint) {
                final String endpoint = this.getApi().getRevokeTokenEndpoint();
                logger.debug("Logout endpoint = {}", endpoint);
                OAuthRequest request = new OAuthRequest(Verb.POST, endpoint);
                this.getApi().getClientAuthentication().addClientAuthentication(request, this.getApiKey(), this.getApiSecret());
                request.addParameter("refresh_token", tokenToRevoke);
                this.logRequestWithParams("revoke token", request);
                return request;
            }

            public Future<Void> revokeToken(String tokenToRevoke, OAuthAsyncRequestCallback<Void> callback, TokenTypeHint tokenTypeHint) {
                OAuthRequest request = this.createRevokeTokenRequest(tokenToRevoke, tokenTypeHint);
                return this.execute(request, callback, new OAuthRequest.ResponseConverter<Void>() {
                    public Void convert(Response response) throws IOException {
                        checkForErrorRevokeToken(response);
                        response.close();
                        return null;
                    }
                });
            }

            public void revokeToken(String tokenToRevoke, TokenTypeHint tokenTypeHint) throws IOException, InterruptedException, ExecutionException {
                final OAuthRequest request = createRevokeTokenRequest(tokenToRevoke, tokenTypeHint);

                try (Response response = execute(request)) {
                    checkForErrorRevokeToken(response);
                }
            }

            private void checkForErrorRevokeToken(Response response) throws IOException {
                logger.debug("Logout response status code = {}", response.getCode()); //logout endpoint returns 204 - no content
                if (response.getCode() != 200 && response.getCode() != 204) {
                    OAuth2AccessTokenJsonExtractor.instance().generateError(response);
                }
            }
        };
    }
}