package eu.transkribus.client.oidc;

import eu.transkribus.core.model.beans.auth.TrpUser;

import java.util.List;

public interface ITokenStorage {
    List<String> getUsernames(final String authServerUrl);
    String getStoredRefreshToken(final String authServerUrl, final String username);
    void updateStoredRefreshToken(final String authServerUrl, final TrpUser user, String refreshToken);
    void removeStoredRefreshToken(final String authServerUrl, final String username);
}
