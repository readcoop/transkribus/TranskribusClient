package eu.transkribus.client.oidc;

import eu.transkribus.client.util.SessionExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ClientRequestSsoAuthFilter implements ClientRequestFilter {
	private final static Logger logger = LoggerFactory.getLogger(ClientRequestSsoAuthFilter.class);


	// ensure token used is valid for at least 5 seconds
	private final static long MIN_VALIDITY = 5L;
	private final static TimeUnit TIME_UNIT = TimeUnit.SECONDS;
	
	private final TrpSsoHandler ssoHandler;
	private final ITokenStorage tokenStorage;
	
	public ClientRequestSsoAuthFilter(TrpSsoHandler ssoHandler, ITokenStorage tokenStorage) {
		this.ssoHandler = ssoHandler;
		this.tokenStorage = tokenStorage;
	}
	
	public void filter(ClientRequestContext requestContext) throws IOException {
		try {
			final String token = getAccessTokenString(MIN_VALIDITY, TIME_UNIT);
			requestContext.getHeaders().putSingle(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", token));
		} catch (IOException e) {
			throw new IOException("Could not retrieve access token.", e);
		} catch (SessionExpiredException e) {
			logger.error("SSO session has expired or user logged out in browser.", e);
			/*
			 * just do not sign the request and let the server respond 401/403.
			 * throwing an IOException here would result in a ProcessingException
			 * during the request and the TranskribusClient code won't handle that gracefully as "session timeout"
			 */
		}
	}

	public String getAccessTokenString(long minValidity, TimeUnit unit) throws IOException, SessionExpiredException {
		if(!ssoHandler.isAccessTokenValid(minValidity, unit)) {
			final String newRefreshToken = ssoHandler.refreshToken(ssoHandler.getRefreshTokenString());
			tokenStorage.updateStoredRefreshToken(ssoHandler.getKeycloakBaseUrlWithRealm(), ssoHandler.getCurrentUser(), newRefreshToken);
		}
		return ssoHandler.getAccessTokenString();
	}
}
