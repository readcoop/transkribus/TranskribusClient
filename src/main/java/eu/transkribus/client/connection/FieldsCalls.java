package eu.transkribus.client.connection;

import java.util.List;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.client.util.JerseyUtils;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.DocumentSelectionDescriptor;
import eu.transkribus.core.model.beans.LA2InferenceConfig;
import eu.transkribus.core.model.beans.LA2TrainConfig;
import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.model.beans.rest.JobParameters;
import eu.transkribus.core.model.beans.rest.ParameterMap;
import eu.transkribus.core.rest.RESTConst;

public class FieldsCalls {
    private static final Logger logger = LoggerFactory.getLogger(FieldsCalls.class);

    ATrpServerConn conn;

    public FieldsCalls(ATrpServerConn conn) {
        this.conn = conn;
    }

    private WebTarget getBaseInferenceTarget() {
        return conn.baseTarget.path(RESTConst.LAYOUT_PATH);
    }

    private WebTarget getBaseTrainTarget() {
        return conn.baseTarget.path(RESTConst.RECOGNITION_PATH);
    }

    public String trainFields(LA2TrainConfig config) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
        WebTarget t = getBaseTrainTarget().path(RESTConst.LA2_TRAIN_PATH);
        return conn.postEntityReturnObject(t, config, MediaType.APPLICATION_XML_TYPE, String.class, MediaType.APPLICATION_XML_TYPE);
    }

    public String trainTables(LA2TrainConfig config) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
        WebTarget t = getBaseTrainTarget().path(RESTConst.TABLE_TRAIN_PATH);
        return conn.postEntityReturnObject(t, config, MediaType.APPLICATION_XML_TYPE, String.class, MediaType.APPLICATION_XML_TYPE);
    }

    public List<TrpJobStatus> inference(int colId, Integer modelId,
            Double threshold,
            boolean addToPageXML,
            Double approxPolyFrac,
            boolean combineWithBaseLayout,
            boolean keepEmptyRegions,
            boolean splitLines,
            double lineOverlapFraction,
            boolean clusterLinesWithoutRegions,
            List<DocumentSelectionDescriptor> dsds,
            ParameterMap params) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {

        JobParameters jobParams = new JobParameters();
        jobParams.setDocs(dsds);
        jobParams.setParams(params);

        WebTarget t = getBaseInferenceTarget().path(RESTConst.FIELDS_INFERENCE_PATH);
        t = JerseyUtils.queryParam(t, RESTConst.COLLECTION_ID_PARAM, colId);
        t = JerseyUtils.queryParam(t, RESTConst.MODEL_ID_PARAM, modelId);
        t = JerseyUtils.queryParam(t, "threshold", threshold);
        t = JerseyUtils.queryParam(t, "addToPageXML", addToPageXML);
        t = JerseyUtils.queryParam(t, "approxPolyFrac", approxPolyFrac);
        t = JerseyUtils.queryParam(t, "combineWithBaseLayout", combineWithBaseLayout);
        t = JerseyUtils.queryParam(t, "keepEmptyRegions", keepEmptyRegions);
        t = JerseyUtils.queryParam(t, "splitLines", splitLines);
        t = JerseyUtils.queryParam(t, "lineOverlapFraction", lineOverlapFraction);
        t = JerseyUtils.queryParam(t, "clusterLinesWithoutRegions", clusterLinesWithoutRegions);

        return conn.postEntityReturnList(t, jobParams, MediaType.APPLICATION_XML_TYPE,
            new GenericType<List<TrpJobStatus>>(){}, MediaType.APPLICATION_XML_TYPE);
    }

    public List<TrpJobStatus> tableInference(int colId, Integer modelId,
            List<DocumentSelectionDescriptor> dsds,
            ParameterMap params) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {

        JobParameters jobParams = new JobParameters();
        jobParams.setDocs(dsds);
        jobParams.setParams(params);
        WebTarget t = getBaseInferenceTarget().path(RESTConst.FIELDS_TABLE_INFERENCE_PATH);
        t = JerseyUtils.queryParam(t, RESTConst.COLLECTION_ID_PARAM, colId);
        t = JerseyUtils.queryParam(t, RESTConst.MODEL_ID_PARAM, modelId);

        return conn.postEntityReturnList(t, jobParams, MediaType.APPLICATION_XML_TYPE,
            new GenericType<List<TrpJobStatus>>(){}, MediaType.APPLICATION_XML_TYPE);
    }

}
