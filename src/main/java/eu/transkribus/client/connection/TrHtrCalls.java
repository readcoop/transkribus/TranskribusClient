package eu.transkribus.client.connection;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.CollectionUtils;

import eu.transkribus.client.util.JerseyUtils;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.model.beans.DocumentSelectionDescriptor;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.rest.RESTConst;

public class TrHtrCalls {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TrHtrCalls.class);
	
	ATrpServerConn conn;
	
	public TrHtrCalls(ATrpServerConn conn) {
		this.conn = conn;
	}
	
	private WebTarget getBaseTarget() {
		return conn.baseTarget;
	}
	
	private WebTarget getTrHtrBaseTarget(int collId, Integer docId, String pageStr, Integer modelId, String modelName, boolean doNotDeleteWorkDir,
			boolean useExistingLinePolygons, boolean doLinePolygonSimplification, List<String> structures, String b2pBackend) {
		WebTarget target = getBaseTarget().path(RESTConst.RECOGNITION_PATH).path(""+collId).path(""+modelId).path(RESTConst.TRHTR_PATH);
		target = target.queryParam(RESTConst.DOC_ID_PARAM, docId);
		target = target.queryParam(RESTConst.PAGES_PARAM, pageStr);
		target = ATrpServerConn.queryParam(target, RESTConst.MODEL_NAME_PARAM, modelName);
		target = target.queryParam(JobConst.PROP_DO_NOT_DELETE_WORKDIR, doNotDeleteWorkDir);
		target = target.queryParam(JobConst.PROP_USE_EXISTING_LINE_POLYGONS, useExistingLinePolygons);
		target = target.queryParam(JobConst.PROP_DO_LINE_POLYGON_SIMPLIFICATION, doLinePolygonSimplification);
		if(!CollectionUtils.isEmpty(structures)) {
			target = target.queryParam(JobConst.PROP_STRUCTURES, new ArrayList<>(structures).toArray());
		}
		target = JerseyUtils.queryParam(target, JobConst.PROP_B2P_BACKEND, b2pBackend);	
		
		return target;
	}
	
	public String runTrHtr(int collId, Integer docId, String pageStr, String modelName, Integer modelId, boolean doNotDeleteWorkDir,
				boolean useExistingLinePolygons, boolean doLinePolygonSimplification, List<String> structures, String b2pBackend) 
			throws SessionExpiredException, ServerErrorException, IllegalArgumentException, ClientErrorException {
		if (modelId==null) {
			modelId = -1;
		}
		
		WebTarget target = getTrHtrBaseTarget(collId, docId, pageStr, modelId, modelName, doNotDeleteWorkDir, useExistingLinePolygons, doLinePolygonSimplification, structures, b2pBackend);
		
		return conn.postEntityReturnObject(target, null, MediaType.APPLICATION_XML_TYPE, 
				String.class, MediaType.APPLICATION_XML_TYPE);
	}	
	
	public String runTrHtr(int collId, DocumentSelectionDescriptor descriptor, String modelName, Integer modelId, boolean doNotDeleteWorkDir,
				boolean useExistingLinePolygons, boolean doLinePolygonSimplification, List<String> structures, String b2pBackend)
					throws SessionExpiredException, ServerErrorException, ClientErrorException {
		if (modelId==null) {
			modelId = -1;
		}		
		if(descriptor == null || descriptor.getDocId() < 1) {
			throw new IllegalArgumentException("No document selected!");
		}
		
		WebTarget target = getTrHtrBaseTarget(collId, null, null, modelId, modelName, doNotDeleteWorkDir, useExistingLinePolygons, doLinePolygonSimplification, structures, b2pBackend);
		
		return conn.postEntityReturnObject(target, descriptor, MediaType.APPLICATION_JSON_TYPE, 
				String.class, MediaType.TEXT_PLAIN_TYPE);
	}
	
}
