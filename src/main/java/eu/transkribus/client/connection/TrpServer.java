package eu.transkribus.client.connection;

import org.apache.commons.lang.StringUtils;

public enum TrpServer {
	Prod("https://transkribus.eu/TrpServer", "https://account.readcoop.eu/auth/realms/readcoop"),
	Test("https://test.transkribus.eu/TrpServerTesting", "https://account.readcoop.eu/authTesting/realms/readcoop"),
	Staging("https://test.transkribus.eu/TrpServer", "https://account.readcoop.eu/auth/realms/readcoop"),
	Local("http://localhost:8080/TrpServerTesting", "https://account.readcoop.eu/authTesting/realms/readcoop"),
	Dev("https://test.transkribus.eu/TrpServerDev", "https://account.readcoop.eu/authTesting/realms/readcoop");
	private final String uri;
	private final String authServerUri;

	TrpServer(String uri, String authServerUri) {
		this.uri = uri;
		this.authServerUri = authServerUri;
	}

	public String getUriStr() {
		return uri;
	}
	public String getAuthServerUriStr() { return authServerUri; }

	/**
	 * Resolve TrpServer instance from a URI String
	 *
	 * @param uriStr given Transkribus server URI
	 * @param defaultInstance the value to return if no match is found
	 * @return the TrpServer with this uriStr or defaultInstance which may also be null
	 */
	public static TrpServer fromUriStr(final String uriStr, final TrpServer defaultInstance) {
		for(TrpServer s : TrpServer.values()) {
			if(s.getUriStr().equals(uriStr)) {
				return s;
			}
		}
		return defaultInstance;
	}

	public static String resolveAuthServerUriStr(String serverUri) {
		if(StringUtils.isBlank(serverUri)) {
			//do not point to any auth server
			throw new IllegalArgumentException("Server URL must not be empty.");
		}
		TrpServer server  = TrpServer.fromUriStr(serverUri, null);
		if(server != null) {
			//return if there's a match
			return server.getAuthServerUriStr();
		}
		//assume prod for anything on our domain
		if(serverUri.startsWith("https://transkribus.eu/")) {
			return Prod.getAuthServerUriStr();
		} else {
			return Test.getAuthServerUriStr();
		}
	}
}