package eu.transkribus.client.connection;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.transkribus.client.util.JerseyUtils;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.TrpDbTag;
import eu.transkribus.core.model.beans.enums.SearchType;
import eu.transkribus.core.model.beans.job.TrpJobStatus;
import eu.transkribus.core.model.beans.rest.JaxbList;
import eu.transkribus.core.model.beans.searchresult.FulltextSearchResult;
import eu.transkribus.core.model.beans.searchresult.KeywordSearchResult;
import eu.transkribus.core.model.beans.searchresult.PageHit;
import eu.transkribus.core.model.beans.searchresult.SearchReplaceParams;
import eu.transkribus.core.model.beans.searchresult.TagSearchResult;
import eu.transkribus.core.rest.RESTConst;
import eu.transkribus.core.util.JaxbUtils;

public class SearchCalls {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SearchCalls.class);
	
	ATrpServerConn conn;
	
	public SearchCalls(ATrpServerConn conn) {
		this.conn = conn;
	}
	
	private WebTarget getBaseTarget() {
		return conn.baseTarget.path(RESTConst.SEARCH_PATH);
	}	
	
	public Future<FulltextSearchResult> searchFulltextAsync(String query,
			SearchType type,
			Integer start,
			Integer rows,
			final List<String> filters,
			Boolean useLegacyIndex,
			InvocationCallback<FulltextSearchResult> callback
			) throws SessionExpiredException, ServerErrorException, ClientErrorException, UnsupportedEncodingException { 
		WebTarget target = getBaseTarget().path(RESTConst.FULLTEXT_PATH);
		
		target = target	.queryParam(RESTConst.QUERY_PARAM, query)	
						.queryParam(RESTConst.TYPE_PARAM, type.toString());
		if(start != null) {
			target = target.queryParam(RESTConst.START_PARAM, start);
		}
		if(rows != null) {
			target = target.queryParam(RESTConst.ROWS_PARAM, rows);
		}
		if(filters != null && !filters.isEmpty()) {
			for(String f : filters){
				target = target.queryParam(RESTConst.FILTER_PARAM, f);
			}	
		}
		if (useLegacyIndex!=null) {
			target = target.queryParam(RESTConst.LEGACY_PARAM, useLegacyIndex);
		}
//		return super.getObject(target, FulltextSearchResult.class, MediaType.APPLICATION_JSON_TYPE);
		return target.request(MediaType.APPLICATION_JSON_TYPE).async().get(callback);
	}
	
	public void searchReplaceFulltext(
			Integer colId,
			String query,
			SearchType type,
			Integer start,
			Integer rows,
			final List<String> filters,
			Boolean useLegacyIndex,
			String replaceTerm
			) throws SessionExpiredException, ServerErrorException, ClientErrorException, UnsupportedEncodingException { 
		WebTarget target = getBaseTarget().path(""+colId).path(RESTConst.SEARCH_REPLACE_PATH);
		
		target = target	.queryParam(RESTConst.QUERY_PARAM, query)	
						.queryParam(RESTConst.TYPE_PARAM, type.toString());
		if(start != null) {
			target = target.queryParam(RESTConst.START_PARAM, start);
		}
		if(rows != null) {
			target = target.queryParam(RESTConst.ROWS_PARAM, rows);
		}
		if(filters != null && !filters.isEmpty()) {
			for(String f : filters){
				target = target.queryParam(RESTConst.FILTER_PARAM, f);
			}	
		}
		if (useLegacyIndex!=null) {
			target = target.queryParam(RESTConst.LEGACY_PARAM, useLegacyIndex);
		}
		target = target.queryParam(RESTConst.REPLACE_TERM, replaceTerm);
		conn.postNullReturnObject(target, TrpJobStatus.class);
		//conn.postEntity(target, TrpJobStatus.class, MediaType.APPLICATION_JSON_TYPE);
		//return conn.getObject(target, TrpJobStatus.class);
	}
	
	public Future<KeywordSearchResult> searchKWAsync(
			String keyword,
			Integer start,
			Integer rows,
			Float probL,
			Float probH,
			final List<String> filters,
			String sorting,
			Integer fuzzy,
			InvocationCallback<KeywordSearchResult> callback
			){
		
		WebTarget target = getBaseTarget().path(RESTConst.KEYWORD_PATH);
		
		target = target	.queryParam(RESTConst.QUERY_PARAM, keyword);
		if(start != null) {
			target = target.queryParam(RESTConst.START_PARAM, start);
		}
		if(rows != null) {
			target = target.queryParam(RESTConst.ROWS_PARAM, rows);
		}		
		if(probL != null) {
			target = target.queryParam(RESTConst.PROBL_PARAM, probL);
		}
		if(probH != null) {
			target = target.queryParam(RESTConst.PROBH_PARAM, probH);
		}		
		if(filters != null && !filters.isEmpty()) {
			for(String f : filters){
				target = target.queryParam(RESTConst.FILTER_PARAM, f);
			}	
		}
		if(fuzzy != null) {
			target = target.queryParam(RESTConst.FUZZY_PARAM, fuzzy);
		}
		if(sorting != null) {
			target = target.queryParam(RESTConst.SORTING_PARAM, sorting);
		}
		
		return target.request(MediaType.APPLICATION_JSON_TYPE).async().get(callback);
	}
	
	@Deprecated
	public FulltextSearchResult searchFulltext(String query,
			SearchType type,
			Integer start,
			Integer rows,
			final List<String> filters
			) throws SessionExpiredException, ServerErrorException, ClientErrorException { 
		WebTarget target = getBaseTarget().path(RESTConst.FULLTEXT_PATH);

		target = target	.queryParam(RESTConst.QUERY_PARAM, query)	
						.queryParam(RESTConst.TYPE_PARAM, type.toString());
		if(start != null) {
			target = target.queryParam(RESTConst.START_PARAM, start);
		}
		if(rows != null) {
			target = target.queryParam(RESTConst.ROWS_PARAM, rows);
		}
		
		target = target.queryParam(RESTConst.LEGACY_PARAM, false);
		if(filters != null && !filters.isEmpty()) {
			for(String f : filters){
				target = target.queryParam(RESTConst.FILTER_PARAM, f);
			}	
		}
		
		return conn.getObject(target, FulltextSearchResult.class, MediaType.APPLICATION_JSON_TYPE);
	}
	
	public WebTarget getSearchTagsTarget(
			Set<Integer> collIds,
			Set<Integer> docIds,
			Set<Integer> pageIds,
			String tagName,
			String tagValue,
			String regionType,
			boolean exactMatch,
			boolean caseSensitive,
			Map<String, Object> attributes,
			boolean useLegacyMethod, 
			Integer start, Integer rows
			) {
		
		Gson gson = new Gson();
		String attributesJson;
		try {
			attributesJson = URLEncoder.encode(gson.toJson(attributes),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unable to encode attributes map: "+e.getMessage());
			attributesJson = null;
		}
		logger.debug("attributesJson = "+attributesJson);
		
		WebTarget t = getBaseTarget().path(RESTConst.TAGS_PATH);
		t = JerseyUtils.queryParam(t, RESTConst.COLLECTION_ID_PARAM, collIds);
		t = JerseyUtils.queryParam(t, RESTConst.DOC_ID_PARAM, docIds);
		t = JerseyUtils.queryParam(t, RESTConst.PAGE_ID_PARAM, pageIds);
		t = JerseyUtils.queryParam(t, RESTConst.TAG_NAME_PARAM, tagName);
		t = JerseyUtils.queryParam(t, RESTConst.TAG_VALUE_PARAM, tagValue);
		t = JerseyUtils.queryParam(t, RESTConst.REGION_TYPE_PARAM, regionType);
		t = JerseyUtils.queryParam(t, RESTConst.EXACT_MATCH_PARAM, exactMatch);
		t = JerseyUtils.queryParam(t, RESTConst.CASE_SENSITIVE_PARAM, caseSensitive);
		t = JerseyUtils.queryParam(t, RESTConst.ATTRIBUTES_PARAM, attributesJson);
		t = JerseyUtils.queryParam(t, RESTConst.LEGACY_PARAM, useLegacyMethod);
		t = JerseyUtils.queryParam(t, RESTConst.START_PARAM, start);
		t = JerseyUtils.queryParam(t, RESTConst.ROWS_PARAM, rows);
		
		
		logger.debug("searching tags target uri = "+t.getUri());
		
		return t;
	}
	
	public Future<List<TrpDbTag>> searchTagsAsyncLegacy(
			Set<Integer> collIds,
			Set<Integer> docIds,
			Set<Integer> pageIds,
			String tagName,
			String tagValue,
			String regionType,
			boolean exactMatch,
			boolean caseSensitive,		
			Map<String, Object> attributes, InvocationCallback<List<TrpDbTag>> callback) /*throws SessionExpiredException, ServerErrorException, IllegalArgumentException, ClientErrorException*/ {
		WebTarget t = getSearchTagsTarget(collIds, docIds, pageIds, tagName, tagValue, regionType, exactMatch, caseSensitive, attributes, true, null, null);
		return t.request(ATrpServerConn.DEFAULT_RESP_TYPE).async().get(callback);
	}

	public List<TrpDbTag> searchTagsLegacy(
				Set<Integer> collIds,
				Set<Integer> docIds,
				Set<Integer> pageIds,
				String tagName,
				String tagValue,
				String regionType,
				boolean exactMatch,
				boolean caseSensitive,		
				Map<String, Object> attributes
			) throws SessionExpiredException, ServerErrorException, ClientErrorException {
		WebTarget t = getSearchTagsTarget(collIds, docIds, pageIds, tagName, tagValue, regionType, exactMatch, caseSensitive, attributes, true, null, null);
				
		return conn.getList(t, TrpServerConn.DB_TAG_LIST_TYPE);
	}
	
	public Future<TagSearchResult> searchTagsAsync(
			Set<Integer> collIds,
			Set<Integer> docIds,
			Set<Integer> pageIds,
			String tagName,
			String tagValue,
			String regionType,
			boolean exactMatch,
			boolean caseSensitive,		
			Map<String, Object> attributes, Integer start, Integer rows, InvocationCallback<TagSearchResult> callback) /*throws SessionExpiredException, ServerErrorException, IllegalArgumentException, ClientErrorException*/ {
		WebTarget t = getSearchTagsTarget(collIds, docIds, pageIds, tagName, tagValue, regionType, exactMatch, caseSensitive, attributes, false, start, rows);
		return t.request(ATrpServerConn.DEFAULT_RESP_TYPE).async().get(callback);
	}
	
	public TagSearchResult searchTags(Set<Integer> collIds, Set<Integer> docIds, Set<Integer> pageIds, String tagName,
			String tagValue, String regionType, boolean exactMatch, boolean caseSensitive,
			Map<String, Object> attributes, Integer start, Integer rows)
			throws SessionExpiredException, ServerErrorException, ClientErrorException {
		WebTarget t = getSearchTagsTarget(collIds, docIds, pageIds, tagName, tagValue, regionType, exactMatch,
				caseSensitive, attributes, false, start, rows);

		return conn.getObject(t, TagSearchResult.class);
	}

	public TrpJobStatus replaceStrings(SearchReplaceParams params) throws SessionExpiredException, ServerErrorException, ClientErrorException {
		WebTarget target = getBaseTarget().path(RESTConst.REPLACE_PATH);
		if(logger.isDebugEnabled()) {
			try {
				logger.debug("SearchReplace parameters JSON:\n{}", JaxbUtils.marshalToJsonString(params, true));
			} catch (JAXBException e) {
				logger.error("Could log export parameters as JSON!", e);
			}
		}
		return conn.postEntityReturnObject(target, params, MediaType.APPLICATION_JSON_TYPE,
				TrpJobStatus.class, MediaType.APPLICATION_JSON_TYPE);
	}
	
	/**
	 * Reset index flag for *either* a collection, a document or a single page. If multiple values are != null priority is: pageId -> docId -> collId
	 */
	public String resetIndexFlag(Integer collId, Integer docId, Integer pageId) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		WebTarget t = getBaseTarget().path(RESTConst.RESET_INDEX_PATH);
		if (collId!=null) {
			t = t.queryParam(RESTConst.COLLECTION_ID_PARAM, collId);
		}
		if (docId!=null) {
			t = t.queryParam(RESTConst.DOC_ID_PARAM, docId);
		}
		if (pageId!=null) {
			t = t.queryParam(RESTConst.PAGE_ID_PARAM, pageId);
		}
		
		return conn.postNullReturnObject(t, String.class);
	}
}
