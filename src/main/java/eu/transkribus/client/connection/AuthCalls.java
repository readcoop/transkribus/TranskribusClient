package eu.transkribus.client.connection;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.exceptions.ClientVersionNotSupportedException;
import eu.transkribus.core.model.beans.auth.TrpUserLogin;
import eu.transkribus.core.rest.RESTConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AuthCalls {
    private static final Logger logger = LoggerFactory.getLogger(AuthCalls.class);
    ATrpServerConn conn;

    TrpUserLogin login;

    public AuthCalls(ATrpServerConn conn) {
        this.conn = conn;
    }

    private WebTarget getBaseTarget() {
        return conn.getBaseTarget()
                .path(RESTConst.AUTH_PATH);
    }

    public TrpUserLogin login(final String user, final String pw) throws ClientVersionNotSupportedException, LoginException {
        if (login != null) {
            logout();
        }

        //post creds to /rest/auth/login
        Form form = new Form();
        form = form.param(RESTConst.USER_PARAM, user);
        form = form.param(RESTConst.PW_PARAM, pw);
        WebTarget loginTarget = getBaseTarget().path(RESTConst.LOGIN_PATH);
        login = null;
        try {
            login = conn.postEntityReturnObject(
                    loginTarget,
                    form, MediaType.APPLICATION_FORM_URLENCODED_TYPE,
                    TrpUserLogin.class, MediaType.APPLICATION_JSON_TYPE
            );

            conn.initTargets();
        } catch (TrpClientErrorException e) {
            login = null;
            if (e.getResponse().getStatus() == ClientVersionNotSupportedException.STATUS_CODE) {
                logger.debug("ClientVersionNotSupportedException on login!");
                throw new ClientVersionNotSupportedException(e.getMessage());
            } else {
                throw new LoginException(e.getMessageToUser());
            }
        } catch (IllegalStateException e) {
            login = null;
            logger.error("Login request failed!", e);
            if ("Already connected".equals(e.getMessage()) && e.getCause() != null) {
                /*
                 * Jersey throws an IllegalStateException "Already connected" for a variety of issues where actually no connection can be established.
                 * see https://github.com/jersey/jersey/issues/3000
                 */
                Throwable cause = e.getCause();
                logger.error("'Already connected' caused by: " + cause.getMessage(), cause);
                //override misleading "Already connected" message
                throw new LoginException(cause.getMessage());
            } else {
                throw new LoginException(e.getMessage());
            }
        } catch (Exception e) {
            login = null;
            logger.error("Login request failed!", e);
            throw new LoginException(e.getMessage());
        }
        logger.debug("Logged in as: " + login.toString());

        return login;
    }

    public void logout() throws TrpServerErrorException, TrpClientErrorException {
        try {
            final WebTarget target = getBaseTarget().path(RESTConst.LOGOUT_PATH);
            //just post a null entity. SessionId is added in RequestAuthFilter
            Response resp = target.request().post(null);
            conn.checkStatus(resp, target);
        } catch (SessionExpiredException see) {
            logger.info("Logout failed as session has expired or sessionId is invalid.");
        } finally {
            login = null;
        }
    }

    public void invalidate() throws SessionExpiredException, ServerErrorException, ClientErrorException {
        final WebTarget docTarget = getBaseTarget().path(RESTConst.INVALIDATE_PATH);
        conn.postNull(docTarget);
    }

    public void refreshSession() throws SessionExpiredException, ServerErrorException, ClientErrorException {
        final WebTarget docTarget = getBaseTarget().path(RESTConst.REFRESH_PATH);
        conn.postNull(docTarget);
    }

    public TrpUserLogin getUserDetails() throws SessionExpiredException {
        final WebTarget docTarget = getBaseTarget()
                .path(RESTConst.DETAILS_PATH);
        TrpUserLogin user = conn.getObject(docTarget, TrpUserLogin.class);
        if (login == null) {
            login = user;
        }
        return user;
    }
}
