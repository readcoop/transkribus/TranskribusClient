package eu.transkribus.client.connection;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import eu.transkribus.client.util.JerseyUtils;
import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.client.util.TrpClientErrorException;
import eu.transkribus.client.util.TrpServerErrorException;
import eu.transkribus.core.model.beans.TrpCollection;
import eu.transkribus.core.model.beans.TrpGroundTruthPage;
import eu.transkribus.core.model.beans.TrpModelMetadata;
import eu.transkribus.core.model.beans.rest.TrpModelMetadataList;
import eu.transkribus.core.rest.RESTConst;

public class ModelCalls {
	
	ATrpServerConn conn;
	
	public ModelCalls(ATrpServerConn conn) {
		this.conn = conn;
	}
	
	private WebTarget getBaseModelTarget(String type) {
		return conn.getBaseTarget()
				.path(RESTConst.MODELS_PATH)
				.path(type);
	}
	
	public TrpModelMetadata updateModel(TrpModelMetadata model) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())
				.path(""+model.getModelId());
		return conn.postEntityReturnObject(t, model, MediaType.APPLICATION_JSON_TYPE, TrpModelMetadata.class, MediaType.APPLICATION_JSON_TYPE);
	}
	
	public TrpModelMetadata getModel(TrpModelMetadata model) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())				
				.path(""+model.getModelId());
		return conn.getObject(t, TrpModelMetadata.class);
	}
	
	public void setModelDeleted(TrpModelMetadata model) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())
				.path(""+model.getModelId());
		conn.delete(t);
	}
	
	public TrpModelMetadataList getModels(String type, Integer colId, Integer userId, Integer releaseLevel, String searchTerm, String provider, 
			List<Integer> centuries, List<String> languages, List<String> scriptTypes,
			String creator, String publisher, String docType, int index, int nValues, String sortField, String sortDirection) throws SessionExpiredException {
		WebTarget t = getBaseModelTarget(type)
				.queryParam(RESTConst.COLLECTION_ID_PARAM, colId)
				.queryParam(RESTConst.USER_ID_PARAM, userId)
				.queryParam(RESTConst.FILTER_PARAM, searchTerm)
				.queryParam(RESTConst.RELEASE_LEVEL_PARAM, releaseLevel)
				.queryParam(RESTConst.PROVIDER_PARAM, provider)
				.queryParam(RESTConst.CENTURIES,  centuries == null ? null : centuries.toArray())
				.queryParam(RESTConst.ISO_LANGUAGES, languages == null ? null : languages.toArray())
				.queryParam(RESTConst.SCRIPT_TYPES, scriptTypes == null ? null : scriptTypes.toArray())
				.queryParam(RESTConst.CREATOR, creator)
				.queryParam(RESTConst.PUBLISHER, publisher)
				.queryParam(RESTConst.DOC_TYPE, docType);
		t = JerseyUtils.setPagingParams(t, index, nValues, sortField, sortDirection);
		return conn.getObject(t, TrpModelMetadataList.class, MediaType.APPLICATION_XML_TYPE);
	}
	
	public List<TrpCollection> getModelCollections(TrpModelMetadata model) throws TrpServerErrorException, TrpClientErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())
				.path("" + model.getModelId())
				.path(RESTConst.COLLECTION_PATH);
		return conn.getList(t, TrpServerConn.COL_LIST_TYPE);
	}
	
	public void addModelToCollection(TrpModelMetadata model, TrpCollection collection) throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())
				.path("" + model.getModelId())
				.path(RESTConst.COLLECTION_PATH);
		conn.postEntity(t, collection, MediaType.APPLICATION_JSON_TYPE);
	}
	
	public void removeModelFromCollection(TrpModelMetadata model, int colId) throws TrpClientErrorException, TrpServerErrorException, SessionExpiredException {
		WebTarget t = getBaseModelTarget(model.getType())
				.path("" + model.getModelId())
				.path(RESTConst.COLLECTION_PATH)
				.path("" + colId);
		conn.delete(t);
	}

	public List<TrpGroundTruthPage> getHtrTrainData(final int htrId) throws SessionExpiredException, IllegalArgumentException, ClientErrorException {
		return getHtrData(htrId, RESTConst.TRAIN_GT_PATH);
	}

	public List<TrpGroundTruthPage> getHtrValidationData(final int htrId) throws SessionExpiredException, IllegalArgumentException, ClientErrorException {
		return getHtrData(htrId, RESTConst.VALIDATION_GT_PATH);
	}

	private List<TrpGroundTruthPage> getHtrData(final int htrId, final String gtPath) throws SessionExpiredException, IllegalArgumentException, ClientErrorException {
		final WebTarget target = getBaseModelTarget("text").path("" + htrId)
				.path(gtPath);
		List<TrpGroundTruthPage> gtList = conn.getList(target, TrpServerConn.GROUND_TRUTH_PAGE_LIST_TYPE);
		//FIXME server 2.8.2 seems to not sort this properly
		Collections.sort(gtList);
		return gtList;		
	}
}
