package eu.transkribus.client.connection;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import eu.transkribus.client.util.SessionExpiredException;
import eu.transkribus.core.model.beans.DocumentSelectionDescriptor;
import eu.transkribus.core.rest.JobConst;
import eu.transkribus.core.rest.RESTConst;

public class Text2ImageCalls {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TrHtrCalls.class);
	
	ATrpServerConn conn;
	
	public Text2ImageCalls(ATrpServerConn conn) {
		this.conn = conn;
	}
	
	private WebTarget getBaseTarget() {
		return conn.baseTarget;
	}
	
	private WebTarget getText2ImageBaseTarget(int collId, Integer docId, String pageStr, int modelId, boolean doNotDeleteWorkDir) {
		WebTarget target = getBaseTarget().path(RESTConst.RECOGNITION_PATH).path(""+collId).path(""+modelId).path(RESTConst.TEXT2IMAGE_PATH);
		target = target.queryParam(RESTConst.DOC_ID_PARAM, docId);
		target = target.queryParam(RESTConst.PAGES_PARAM, pageStr);
		target = target.queryParam(JobConst.PROP_DO_NOT_DELETE_WORKDIR, doNotDeleteWorkDir);
		return target;
	}
	
	public String runText2Image(int collId, DocumentSelectionDescriptor descriptor, int modelId, boolean doNotDeleteWorkDir)
					throws SessionExpiredException, ServerErrorException, ClientErrorException {
		if(descriptor == null || descriptor.getDocId() < 1) {
			throw new IllegalArgumentException("No document selected!");
		}
		WebTarget target = getText2ImageBaseTarget(collId, null, null, modelId, doNotDeleteWorkDir);
		
		return conn.postEntityReturnObject(target, descriptor, MediaType.APPLICATION_JSON_TYPE, 
				String.class, MediaType.TEXT_PLAIN_TYPE);
	}
}
